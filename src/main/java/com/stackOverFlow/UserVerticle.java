package com.stackOverFlow;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

public class UserVerticle extends AbstractVerticle {
	
	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer("read.users", message -> {
			System.out.println("UserVerticle.start() read.users message: "+message.body().toString());
			JsonObject jsonMsg = new JsonObject(message.body().toString());
			String username = jsonMsg.getString("username");
			System.out.println("UserVerticle.start() read.users username: " +username);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + WebserverVerticle.MONGODB + ":27017");
			MongoClient client = MongoClient.createShared(vertx, config);
			FindOptions options = new FindOptions().setFields(new JsonObject().put("_id", true)
					.put(MyMongoAuth.DEFAULT_FIRSTNAME_FIELD, true).put(MyMongoAuth.DEFAULT_LASTNAME_FIELD, true)
					.put(MyMongoAuth.DEFAULT_CITY_FIELD, true));
//			client.find("users", new JsonObject().put("username", username), res -> {
			client.findWithOptions("users", new JsonObject().put("_id", username),options, res -> {
				if (res.succeeded()) {
					if (res.result().size() == 0)
						message.fail(404, "User not found");
					else {
						JsonObject user = res.result().get(0);
						String json = Json.encodePrettily(user);
						System.out.println("User Found: " + json);
						message.reply(json);
					}
				} else {
					res.cause().printStackTrace();
					message.fail(500, "User not found");
				}
			});
		});
		
		
		vertx.eventBus().consumer("write.users", message -> {
			JsonObject jsonMsg = new JsonObject(message.body().toString());
			String username = jsonMsg.getString("username");
			String password = jsonMsg.getString("password");
			String firstname = jsonMsg.getString("firstname");
			String lastname = jsonMsg.getString("lastname");
			String city = jsonMsg.getString("city");
			System.out.println("UserVerticle.start() write.users jsonMsg.getValue of username: "+jsonMsg.getValue("username"));
			System.out.println("UserVerticle.start() write.users username: "+username + " password: "+password);
			if(username == null || password == null) {
				message.fail(500, "Registration failed as username or password is empty");
				return;
			}
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + WebserverVerticle.MONGODB + ":27017");
			MongoClient client = MongoClient.createShared(vertx, config);
			JsonObject authProperties = new JsonObject();
			authProperties.put(MongoAuth.PROPERTY_COLLECTION_NAME, "users");
			authProperties.put(MongoAuth.PROPERTY_USERNAME_FIELD, "_id");
			authProperties.put(MongoAuth.DEFAULT_USERNAME_FIELD, username);
			authProperties.put(MongoAuth.DEFAULT_PASSWORD_FIELD, password);
			authProperties.put(MyMongoAuth.DEFAULT_FIRSTNAME_FIELD, firstname);
			authProperties.put(MyMongoAuth.DEFAULT_LASTNAME_FIELD, lastname);
			authProperties.put(MyMongoAuth.DEFAULT_CITY_FIELD, city);
//		    MongoAuth authProvider = MongoAuth.create(client, authProperties);
			MyMongoAuth authProvider = new MyAuthProvider(client, authProperties);
		    authProvider.insertUser(authProperties, null, null, res -> {
		    	if(res.succeeded()) {
		    		message.reply("USER is registered");
		    	} else {
		    		message.fail(500, "Registration failed");
		    	}
		    });
		    
		});
		
	}

}
