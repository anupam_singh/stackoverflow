package com.stackOverFlow;

import java.util.List;
import java.util.Set;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.HashSaltStyle;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.auth.mongo.impl.DefaultHashStrategy;
import io.vertx.ext.auth.mongo.impl.MongoAuthImpl;
import io.vertx.ext.auth.mongo.impl.MongoUser;
import io.vertx.ext.mongo.MongoClient;

public class MyAuthProvider extends MongoAuthImpl implements MyMongoAuth {

	private MongoClient mongoClient;

	private JsonObject config;

	private String firstNameField = DEFAULT_FIRSTNAME_FIELD;

	private String lastNameField = DEFAULT_LASTNAME_FIELD;

	private String cityField = DEFAULT_CITY_FIELD;

	public MyAuthProvider(MongoClient mongoClient, JsonObject config) {
		super(mongoClient, config);
		this.mongoClient = mongoClient;
		this.config = config;
		init();
	}

	private void init() {
		String firstNameField = config.getString(PROPERTY_FIRSTNAME_FIELD);
		if (firstNameField != null) {
			setFirstNameField(firstNameField);
		}

		String lastNameField = config.getString(PROPERTY_LASTNAME_FIELD);
		if (lastNameField != null) {
			setLastNameField(lastNameField);
		}

		String cityField = config.getString(PROPERTY_CITY_FIELD);
		if (cityField != null) {
			setCityField(cityField);
		}
	}

	@Override
	public void insertUser(JsonObject userprofile, List<String> roles, List<String> permissions,
			Handler<AsyncResult<String>> resultHandler) {
		JsonObject principal = new JsonObject();
		System.out.println("MyAuthProvider.insertUser() getUsernameField(): " + getUsernameField()
				+ " userprofile.getString(DEFAULT_USERNAME_FIELD): " + userprofile.getString(DEFAULT_USERNAME_FIELD)
				+ " getPasswordField: " + getPasswordField());
		System.out.println("MyAuthProvider.insertUser() userprofile.getString(DEFAULT_FIRSTNAME_FIELD): "
				+ userprofile.getString(DEFAULT_FIRSTNAME_FIELD));
		principal.put(getUsernameField(), userprofile.getString(DEFAULT_USERNAME_FIELD));
		principal.put(getFirstNameField(), userprofile.getString(DEFAULT_FIRSTNAME_FIELD));
		principal.put(getLastNameField(), userprofile.getString(DEFAULT_LASTNAME_FIELD));
		principal.put(getCityField(), userprofile.getString(DEFAULT_CITY_FIELD));

		if (roles != null) {
			principal.put(MongoAuth.DEFAULT_ROLE_FIELD, new JsonArray(roles));
		}

		if (permissions != null) {
			principal.put(MongoAuth.DEFAULT_PERMISSION_FIELD, new JsonArray(permissions));
		}
		MongoUser user = new MongoUser(principal, this);

		if (getHashStrategy().getSaltStyle() == HashSaltStyle.COLUMN) {
			principal.put(getSaltField(), DefaultHashStrategy.generateSalt());
		}

		String cryptPassword = getHashStrategy().computeHash(userprofile.getString(getPasswordField()), user);
		principal.put(getPasswordField(), cryptPassword);

		// mongoClient.insert(getCollectionName(), userprofile, res -> {
		// System.out.println("MyAuthProvider.insertUser() res: "+res);
		// resultHandler.handle(res);
		// });
		System.out.println("MyAuthProvider.insertUser() user.principal(): "+user.principal().encodePrettily());
		mongoClient.save(getCollectionName(), user.principal(), resultHandler);

	}

	@Override
	public MyMongoAuth setFirstNameField(String firstname) {
		this.firstNameField = firstname;
		return this;
	}

	@Override
	public MyMongoAuth setLastNameField(String lastname) {
		this.lastNameField = lastname;
		return this;
	}

	@Override
	public MyMongoAuth setCityField(String city) {
		this.cityField = city;
		return this;
	}

	@Override
	public String getFirstNameField() {
		return this.firstNameField;
	}

	@Override
	public String getLastNameField() {
		return lastNameField;
	}

	@Override
	public String getCityField() {
		return cityField;
	}

}
