package com.stackOverFlow;

import java.util.List;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public interface MyMongoAuth {
	
	String DEFAULT_FIRSTNAME_FIELD = "firstname";
	
	String DEFAULT_LASTNAME_FIELD = "lastname";
	
	String DEFAULT_CITY_FIELD = "city";
	
	String PROPERTY_FIRSTNAME_FIELD = "firstnamefield";
	
	String PROPERTY_LASTNAME_FIELD = "lastnamefield";
	
	String PROPERTY_CITY_FIELD = "cityfield";

	void insertUser(JsonObject userprofile, List<String> roles, List<String> permissions,
			Handler<AsyncResult<String>> resultHandler);
	
	MyMongoAuth setFirstNameField(String firstname);
	
	MyMongoAuth setLastNameField(String lastname);
	
	MyMongoAuth setCityField(String city);
	
	String getFirstNameField();
	
	String getLastNameField();
	
	String getCityField();

}
