package com.stackOverFlow;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

public class QAPostVerticle extends AbstractVerticle {
	
	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer("post.question", message -> {
			System.out.println("QAPostVerticle.start() message.body().toString(): "+message.body().toString());
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + WebserverVerticle.MONGODB + ":27017");
			MongoClient client = MongoClient.createShared(vertx, config);
			JsonObject object = new JsonObject(message.body().toString());
			client.insert("questions", object, res -> {
				if (res.succeeded()) {
					message.reply("Question is POSTED with id " + res.result());
				} else {
					message.fail(500, "Question Posting failed");
				}
			});
		});
		vertx.eventBus().consumer("post.answer", message -> {
			JsonObject msgJsonObj = new JsonObject(message.body().toString());
			String questionId = msgJsonObj.getString("questionId");
			System.out.println("QAPostVerticle.start() questionId: "+questionId);
			JsonObject answerObj = msgJsonObj.getJsonObject("answer");
			System.out.println("QAPostVerticle.start() answerObj.encodePrettily(): "+answerObj.encodePrettily());
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + WebserverVerticle.MONGODB + ":27017");
			MongoClient client = MongoClient.createShared(vertx, config);
//			JsonObject object = new JsonObject(message.body().toString());
//			JsonObject update = new JsonObject().put("$push", new JsonObject().put("answers", new JsonObject().put("$each", answerObj)));
			JsonObject update = new JsonObject().put("$push", new JsonObject().put("answers", answerObj));
			System.out.println("QAPostVerticle.start() update.encodePrettily(): "+update.encodePrettily());
			JsonObject query = new JsonObject().put("_id", questionId);
			client.updateCollection("questions", query,update, res -> {
				if (res.succeeded()) {
					message.reply("Question is POSTED with id " + res.result());
				} else {
					message.fail(500, "Question Posting failed");
				}
			});
		});
	}

}
