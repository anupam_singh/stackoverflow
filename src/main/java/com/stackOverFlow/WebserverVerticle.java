package com.stackOverFlow;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.FormLoginHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;

public class WebserverVerticle extends AbstractVerticle {
	
//	public static final String MONGODB = "localhost";
	public static final String MONGODB = "mongodbhost";
	
	@Override
	public void start(Future<Void> future) throws Exception {
		Router router = Router.router(vertx);
		vertx.deployVerticle("com.stackOverFlow.UserVerticle", new DeploymentOptions().setWorker(true).setInstances(2));
		vertx.deployVerticle("com.stackOverFlow.QAPostVerticle", new DeploymentOptions().setWorker(true).setInstances(2));
		vertx.deployVerticle("com.stackOverFlow.QAReadVerticle", new DeploymentOptions().setWorker(true).setInstances(2));
		router.route().handler(CookieHandler.create());
		router.route().handler(BodyHandler.create());
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
		
		// Create a JWT Auth Provider
	    JWTAuth jwt = JWTAuth.create(vertx, new JsonObject()
	        .put("keyStore", new JsonObject()
	            .put("type", "jceks")
	            .put("path", "keystore.jceks")
	            .put("password", "secret")));
	    
	    JsonObject config = new JsonObject();
		config.put("db_name", "stackoverflow");
//		config.put("connection_string", "mongodb://localhost:27017");
		config.put("connection_string", "mongodb://" + MONGODB + ":27017");
	    MongoClient client = MongoClient.createShared(vertx, config);
	    JsonObject authProperties = new JsonObject();
	    authProperties.put(MongoAuth.PROPERTY_COLLECTION_NAME, "users");
		authProperties.put(MongoAuth.PROPERTY_USERNAME_FIELD, "_id");
	    MongoAuth authProvider = MongoAuth.create(client, authProperties);
//	    authProvider.setCollectionName("users");
	    
	 // protect the API
//	    router.route("/private/*").handler(JWTAuthHandler.create(jwt));
	    Handler<RoutingContext> handler = JWTAuthHandler.create(jwt);
	    router.route(HttpMethod.GET,"/users/").handler(handler);
	    router.route(HttpMethod.POST,"/logout/").handler(handler);
	    router.route(HttpMethod.POST,"/questions/").handler(handler);
//	    router.route(HttpMethod.POST,"/answers/:questionId").handler(handler);
	    router.route(HttpMethod.POST,"/questions/:questionId/answers").handler(handler);
	    
	    router.route("/login").handler(new CustomFormLoginHandler(authProvider, FormLoginHandler.DEFAULT_USERNAME_PARAM, FormLoginHandler.DEFAULT_PASSWORD_PARAM, FormLoginHandler.DEFAULT_RETURN_URL_PARAM, null,jwt));
	    
	    router.post("/users/").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() POST USERS rctx.getBodyAsJson().encodePrettily(): "+rctx.getBodyAsJson().encodePrettily());
			vertx.eventBus().send("write.users", rctx.getBodyAsJson().encodePrettily(), res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.get("/users/").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() users rctx: "+rctx);
//	    	System.out.println("WebserverVerticle.start() GET USERS rctx.getBodyAsJson(): "+rctx.getBodyAsJson());
//	    	System.out.println("WebserverVerticle.start() GET USERS rctx.getBodyAsJson().encodePrettily(): "+rctx.getBodyAsJson().encodePrettily());
	    	System.out.println("WebserverVerticle.start() GET USERS rctx.user(): "+rctx.user());
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	System.out.println("WebserverVerticle.start() rctx.user().principal(): "+rctx.user().principal());
			vertx.eventBus().send("read.users", rctx.user().principal(), res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.get("/questions/").handler(rctx -> {
	    	vertx.eventBus().send("read.Question",null, res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.get("/questions/results/").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() search_query: "+rctx.request().getParam("search_query"));
	    	JsonObject jsonObj = new JsonObject().put("search_query", rctx.request().getParam("search_query"));
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.get("/questions/id/:questionId").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() questions/:questionId");
	    	JsonObject jsonObj = new JsonObject().put("questionId", rctx.request().getParam("questionId"));
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.post("/questions/").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() GET USERS rctx.user(): "+rctx.user());
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	System.out.println("WebserverVerticle.start() rctx.user().principal(): "+rctx.user().principal());
	    	vertx.eventBus().send("read.users", rctx.user().principal(), res -> {
	    		System.out.println("WebserverVerticle.start() res.result().body().toString(): "+res.result().body().toString());
				if(res.succeeded()) {
//					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
//					.end(res.result().body().toString());
					JsonObject userJsonObj = new JsonObject(res.result().body().toString());
					userJsonObj.remove(MongoAuth.DEFAULT_PASSWORD_FIELD);
					userJsonObj.remove(MongoAuth.DEFAULT_SALT_FIELD);
					userJsonObj.put(MongoAuth.DEFAULT_USERNAME_FIELD, userJsonObj.getString("_id"));
					userJsonObj.remove("_id");
					JsonObject questionObj = rctx.getBodyAsJson();
					System.out.println("WebserverVerticle.start() questionObj: "+questionObj.encodePrettily() );
					questionObj.put("user", userJsonObj);
					System.out.println("WebserverVerticle.start() AFTERRRR questionObj: "+questionObj.encodePrettily() );
					vertx.eventBus().send("post.question", questionObj.encodePrettily(), result -> {
						if(result.succeeded()) {
							rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(result.result().body().toString());
						} else if(res.failed()) {
							rctx.response().setStatusCode(((ReplyException)result.cause()).failureCode()).putHeader("Content-Type", "application/json")
							.end(result.cause().getLocalizedMessage());
						}
					});
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.post("/questions/:questionId/answers").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() GET USERS rctx.user(): "+rctx.user());
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	System.out.println("WebserverVerticle.start() rctx.user().principal(): "+rctx.user().principal());
	    	
	    	vertx.eventBus().send("read.users", rctx.user().principal(), res -> {
	    		System.out.println("WebserverVerticle.start() res.result().body().toString(): "+res.result().body().toString());
				if(res.succeeded()) {
//					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
//					.end(res.result().body().toString());
					JsonObject userJsonObj = new JsonObject(res.result().body().toString());
					userJsonObj.remove(MongoAuth.DEFAULT_PASSWORD_FIELD);
					userJsonObj.remove(MongoAuth.DEFAULT_SALT_FIELD);
					userJsonObj.put(MongoAuth.DEFAULT_USERNAME_FIELD, userJsonObj.getString("_id"));
					userJsonObj.remove("_id");
					JsonObject tmpAnswerObj = rctx.getBodyAsJson();
					tmpAnswerObj.put("user", userJsonObj);
					JsonObject answerObj = new JsonObject();
					answerObj.put("questionId", rctx.request().getParam("questionId"));
					answerObj.put("answer", tmpAnswerObj);
					vertx.eventBus().send("post.answer", answerObj.encodePrettily(), result -> {
						if(result.succeeded()) {
							rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(result.result().body().toString());
						} else if(result.failed()) {
							rctx.response().setStatusCode(((ReplyException)result.cause()).failureCode()).putHeader("Content-Type", "application/json")
							.end(result.cause().getLocalizedMessage());
						}
					});
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
	    
	    router.post("/logout/").handler(rctx -> {
	    	System.out.println("WebserverVerticle.start() logout rctx: "+rctx);
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	rctx.clearUser();
	    	rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json").end("User logged out");
		});
	    vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", 8080),
				result -> {
					if (result.succeeded()) {
						future.complete();
					} else {
						future.fail(result.cause());
					}
				});
	}

}
