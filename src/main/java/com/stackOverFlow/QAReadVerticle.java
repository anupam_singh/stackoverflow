package com.stackOverFlow;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

public class QAReadVerticle extends AbstractVerticle {

	@Override
	public void start() throws Exception {

		vertx.eventBus().consumer("read.Question", message -> {
			JsonObject queryObj = new JsonObject();
			FindOptions options = new FindOptions().setFields(
					new JsonObject().put("_id", true).put("title", true).put("description", true).put("user", true));
			if (message != null && message.body() != null && message.body().toString().length() > 0) {
				JsonObject msgJsonObj = new JsonObject(message.body().toString());
				String questionId = msgJsonObj.getString("questionId");
				String search_query = msgJsonObj.getString("search_query");
				System.out.println("QAReadVerticle.start() questionId: " + questionId);
				System.out.println("QAReadVerticle.start() search_query: " + search_query);
				if (questionId != null && questionId.length() > 0) {
					options.getFields().put("answers", true);
					queryObj = new JsonObject().put("_id", questionId);
				} else if (search_query != null && search_query.length() > 0) {
					queryObj = new JsonObject().put("title",
							new JsonObject().put("$regex", ".*" + search_query + ".*").put("$options", "i"));
				}
			}
			System.out.println("QAReadVerticle.start() queryObj: " + queryObj.encodePrettily());
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + WebserverVerticle.MONGODB + ":27017");
			MongoClient client = MongoClient.createShared(vertx, config);
			client.findWithOptions("questions", queryObj, options, res -> {
				if (res.succeeded()) {
					if (res.result().size() == 0)
						message.fail(404, "NO Questions found");
					else {
						message.reply(new JsonArray(res.result()));
					}
				} else {
					res.cause().printStackTrace();
					message.fail(500, "NO Questions found");
				}
			});
		});
	}

}
